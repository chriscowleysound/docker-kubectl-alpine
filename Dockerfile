FROM quay.io/chriscowley/alpine:3.13.5

RUN apk --no-cache add curl && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.21.1/bin/linux/amd64/kubectl
RUN mv kubectl /usr/local/bin/kubectl && chmod +x /usr/local/bin/kubectl
